<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<script src="http://momentjs.com/downloads/moment-with-locales.js"></script>
<script src="http://momentjs.com/downloads/moment-timezone-with-data.js"></script>

<style>
body{
  margin-left: 20%;
}
.reservation{
  border: 2px solid black;
  display: block;
  margin-left: auto;
  margin-right: auto;
 
}
.title{
    margin-left: 50%;

}
.intro{
    display:flex;
    justify-content: space-around;
}
.selectsalle{
  border: 2px solid red;
  display:flex;
}
</style>
</head>
<body>
<div class="intro">
   <div><h1 class="title">RESERVATION</h1></div>
<div>
  <body onload="startTime()">
    <p id="demo"></p>
   <div id="txt"></div>
   </div>
</div>

<hr>
<form class="form-horizontal container reservation col-lg-10" method="post" action="{{action('EventController@store')}}">
@csrf
<div>
    <div class="alert alert-primary" role="alert">
      ENTREPRISE
   </div>

</div>

<h5>Salle:</h5>
   
           
<div class="input-group selectsalle container col-lg-12">
  <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01">Sélectionner une salle</label>
  </div>
  <select class="custom-select " id="inputGroupSelect01" name="salle_id">
 <?php foreach ($salle as $salle) {
?>
     <option value="<?php echo $salle->id; ?>"><?php echo $salle->nom_salle; ?></option>
 <?php }
?>
  </select>

</div>

<br>

<h5>Entreprise:</h5>

        
<div class="input-group selectsalle container col-lg-12">
  <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01">Sélectionner une salle</label>
  </div>
  <select class="custom-select " id="inputGroupSelect01" name="utilisateur_id">
 <?php foreach ($utilisateur as $utilisateur) {
?>
     <option value="<?php echo $utilisateur->id; ?>"><?php echo $utilisateur->nom_entreprise; ?></option>
 <?php }
?>
  </select>

</div>

<div class="container" style="margin-top:70px;">
<div class="row">
       
            <div class="form-group registration-date">
                <label class="control-label col-sm-3" for="registration-date">Date début:</label>
            <div class="input-group registration-date-time">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
            <input class="form-control" name="date_debut" id="registration-date" type="date">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></span>
            <input class="form-control" name="heure_debut" id="registration-time" type="time">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button" onclick="addNow()"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Now</button>
                    <button class="btn btn-default" type="button" onclick="stopNow()"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Stop</button>
                    </span>
            </div>
            </div>
       
</div>
</div>


<div class="container" style="margin-top:30px;">
<div class="row">
       
            <div class="form-group registration-date">
                <label class="control-label col-sm-3" for="registration-date">Date fin:</label>
            <div class="input-group registration-date-time">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
            <input class="form-control" name="date_fin" id="registration-date" type="date">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-time" aria-hidden="true"></span></span>
            <input class="form-control" name="heure_fin" id="registration-time" type="time">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button" onclick="addNow()"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Now</button>
                    <button class="btn btn-default" type="button" onclick="stopNow()"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Stop</button>
                    </span>
            </div>
            </div>
       
</div>
</div>



<button type="submit" class="btn btn-primary">Réserver</button>

</form>
<script>
function addNow() {
  nowDate = moment().tz("Europe/London").format('YYYY-MM-DD');
  nowTime = moment().tz("Europe/London").format('HH:mm:ss');
  document.getElementById('registration-date').value = nowDate;
  document.getElementById('registration-time').value = nowTime;
  set = setTimeout(function () { addNow(); }, 1000);
}

function stopNow() {
  clearTimeout(set);
}


function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>
</body>
</html> 
