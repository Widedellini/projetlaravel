<html>
<head>



<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



</head>
<body>
<center><h1>LISTE DES ENTREPRISES</h1></center><br>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Ajouter une entreprise
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter une nouvelle Entreprise</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{action('UtilisateurController@store')}}">
          @csrf
      <div class="modal-body">
        <span >Nom Entreprise:</span>
        <input type="text" name="nom_entreprise" /><br>
        <span >Email :</span>
        <input type="text" name="email" /><br>
        <span >Login :</span>
        <input type="text" name="login" /><br>
        <span >Code :</span>
        <input type="text" name="code"/>

      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    </form>
  </div>
</div>



<table class="table table-striped  container">
  <thead>
    <tr>
      <th scope="col">Nom entreprise</th>
      <th scope="col">Email</th>
      <th scope="col">Login</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($utilisateur as $utilisateur)
    <tr>
    <td>{{$utilisateur->nom_entreprise}}</td>
    <td>{{$utilisateur->email}}</td>
    <td>{{$utilisateur->login}}</td>
    <td>
        <!--<form action="{{action('UtilisateurController@update', $utilisateur->id)}}" method="post">
            @csrf
            <!-- Modal -->
<!--<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="post" action="{{action('UtilisateurController@store', $utilisateur->id)}}">
            @csrf
        <div class="modal-body">
          <span>Logo:</span>
          <input type="text" name="logo" value={{ $utilisateur->logo }} /><br>
          <span >Nom Entreprise:</span>
          <input type="text" name="nom_entreprise" value={{ $utilisateur->nom_entreprise }}/><br>
          <span >Email :</span>
          <input type="text" name="email" value={{ $utilisateur->email }}/><br>
          <span >Login :</span>
          <input type="text" name="login" value={{ $utilisateur->login }}/><br>
          <span >Code :</span>
          <input type="text" name="code" value={{ $utilisateur->code }}/>
  
        </div>
       
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      </form>
    </div>
  </div>
            <!-- Button trigger modal -->
<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">
  Edit
</button>-->
<a href="{{action('UtilisateurController@edit', $utilisateur->id)}}" class="btn btn-primary">Edit</a>
          </form>
    </td>
    <td>
        <form action="{{action('UtilisateurController@destroy', $utilisateur->id)}}" method="post">
          @csrf
          <button class="btn btn-danger" type="submit">Delete</button>
        </form>
    </td>
    </tr>
        @endforeach
  </tbody>
</table>







</body>
</html>
