<html>
<head>



<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>



</head>
<body>

<center><h1>LISTE DES SALLES</h1></center><br>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" href="{{ route('SalleReunion.create')}}">
  Ajouter une Salle
</button>

<!-- ajoutModal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter une nouvelle Salle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form method="post" action="{{action('SalleController@store')}}">
        @csrf
      <div class="modal-body">
        <span>Salle</span>
        <input type="text" name="nom_salle" /><br>
        <span >Etat:</span>
        <input type="text" name="etat" /><br>
        <span >Information:</span>
        <input type="textarea" name="description" /><br>
       

      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
    </form>
  </div>
</div>





<table class="table table-striped  container">
  <thead>
    <tr>
      <th scope="col">  Salle</th>
      <th scope="col">Etat</th>
      <th scope="col">Information</th>
    </tr>
    </thead>
    <tbody>
        @foreach($salle as $salle)
    <tr>
    <td>{{$salle->nom_salle}}</td>
    <td>{{$salle->description}}</td>
    <td>{{$salle->etat}}</td>
    <td>
      <!--<div class="btn-modal">
      
        
        <form method="post" action="{{ route('SalleReunion.update', $salle->id)}}">
          @csrf
        <div class="modal-body">
          <span>Salle</span>
          <input type="text" name="nom_salle" value={{ $salle->nom_salle }} /><br>
          <span >Etat:</span>
          <input type="text" name="description" value={{ $salle->description }} /><br>
          <span >Information:</span>
          <input type="textarea" name="etat" value={{ $salle->etat }}/><br>
         
  
        </div>
       
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      </form>
    <button type="button" class="btn btn-primary" name="edit" data-toggle="modal" data-target="#editModal" href="{{ route('SalleReunion.edit',['id' => $salle->id])}}">
      Edit
    </button>
  </div>-->
<a href="{{action('SalleController@edit', $salle->id)}}" class="btn btn-primary">Edit</a>
</td>
  <td>
      <form action="{{ route('SalleReunion.destroy', $salle->id)}}" method="post" >
        @csrf
        <button class="btn btn-danger" type="submit">Delete</button>
      </form>
  </td>
    </tr>
        @endforeach
          <!-- EditModal -->
<!--<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>-->
    </tbody>
  </table>
</html>
