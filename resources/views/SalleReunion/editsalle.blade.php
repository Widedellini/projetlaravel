@extends('base')
<html>
<head>

</head>
<body>
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <center><h1 class="display-5">Update Salle</h1></center>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{action('SalleController@update', $salle->id)}}">
            @csrf
            <div class="form-group">

                <label for="first_name">Nom Salle :</label>
                <input type="text" class="form-control" name="nom_salle" value={{ $salle->nom_salle }} />
            </div>

            <div class="form-group">
                <label for="last_name">Etat :</label>
                <input type="text" class="form-control" name="description" value={{ $salle->etat }} />
            </div>

            <div class="form-group">
                <label for="email">Description :</label>
                <input type="textarea" class="form-control" name="etat" value={{ $salle->description }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>

</body>
</html>

