@extends('base')
<html>
<head>

</head>
<body>
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <center><h1 class="display-5">Update Entreprise</h1></center>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{URL::action('UtilisateurController@update', $utilisateur->id)}}">
             @csrf
            <div class="form-group">

                <label for="first_name">Nom Entreprise :</label>
                <input type="text" class="form-control" name="nom_entreprise" value={{ $utilisateur->nom_entreprise }} />
            </div>

            <div class="form-group">
                <label for="last_name">Email :</label>
                <input type="text" class="form-control" name="email" value={{ $utilisateur->email }} />
            </div>

            <div class="form-group">
                <label for="email">Login :</label>
                <input type="text" class="form-control" name="login" value={{ $utilisateur->login }} />
            </div>

            <div class="form-group">
                <label for="email">code :</label>
                <input type="text" class="form-control" name="code" value={{ $utilisateur->code }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>

</body>
</html>

