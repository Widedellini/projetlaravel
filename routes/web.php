<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TestLoginController@index')->name('SalleReunion.TestLogin');
Route::get('/des', 'ReservationUtilisController@index')->name('SalleReunion.GesReserSalleEntr');

Route::get('/sa', 'SalleController@index')->name('SalleReunion.GestionSalle');
Route::post('/sall', 'SalleController@store')->name('SalleReunion.store');
Route::get('/ListeSalle', 'SalleController@create')->name('SalleReunion.create');
Route::get('/ListeSall/{id}', 'SalleController@edit')->name('SalleReunion.edit');
Route::post('/ListeSal/{id}', 'SalleController@update')->name('SalleReunion.update');
Route::post('/ListeSaldfgh/{id}', 'SalleController@destroy')->name('SalleReunion.destroy');
Route::get('/salle', 'SalleController@listesalle')->name('SalleReunion.listesalle');



Route::get('/ListeEntreprise', 'UtilisateurController@index')->name('SalleReunion.ListeEntreprise');
Route::post('/ListeEntrepris', 'UtilisateurController@store')->name('SalleReunion.store');
Route::get('/ListeEntrepri', 'UtilisateurController@create')->name('SalleReunion.create');
Route::get('/ListeEntrepr/{id}', 'UtilisateurController@edit')->name('SalleReunion.edit');
Route::post('/ListeEntre/{id}', 'UtilisateurController@update')->name('SalleReunion.update');
Route::post('/ListeEntr/{id}', 'UtilisateurController@destroy')->name('SalleReunion.destroy');


Route::post('/ListeReservatio', 'ReservationUtilisController@store')->name('ResSalleReunion.store');
Route::get('/ListeReservation', 'ReservationUtilisController@form')->name('SalleReunion.Reservation');
Route::post('/ListeReservati', 'ReservationUtilisController@create')->name('ResSalleReunion.create');
Route::post('/ListeReservat', 'ReservationUtilisController@test')->name('SalleReunion.Reservation');

Route::get('/events', 'EventController@index')->name('SalleReunion.mycalender');
Route::post('/ListeReazert', 'EventController@store')->name('SalleReunionn.store');
Route::get('/ListeReson', 'EventController@form')->name('SalleReunion.Reservation');
Route::get('/Liseservati', 'EventController@create')->name('SalleReunion.create');



