<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
    protected $fillable = [
        'nom_salle',
        'description',
        'etat'    
    ];  
    protected $dates = [ 'created_at','updated_at'];

    public function event(){
    return $this->hasMany(Event::class);
}}
