<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model

{

    protected $fillable = [
        'date_debut',
        'heure_debut',
        'date_fin',
        'heure_fin',
        'type',
        'salle_id',
        'utilisateur_id'  
    ];

     public function salle(){
         return $this->hasOne(salle::class,'id','salle_id');
    }
   public function utilisateur(){
         return $this->hasOne(utilisateur::class,'id','utilisateur_id');
    }
}
