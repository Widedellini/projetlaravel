<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Event;
use App\Utilisateur;
use App\Salle;
use Illuminate\Http\Request;

class SalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('SalleReunion.GestionSalle', compact('salle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salle = Salle::all(); 
       return view('SalleReunion.salle', compact('salle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'nom_salle'=>'required',
            'description'=>'required',
            'etat'=>'required'
        ]);

        $salle = new salle([
            'nom_salle' => $request->get('nom_salle'),
            'description' => $request->get('description'),
            'etat' => $request->get('etat')
        ]);
        $salle->save();
        
        return redirect('ListeSalle')->with('success', 'salle saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function show(Salle $salle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function edit(salle $salle, $id)
    {
       
        $salle = Salle::findOrFail($id);
        return view('SalleReunion.editsalle', compact('salle'));  
}

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $salle=Salle::findOrFail($id);
        $salle->nom_salle=$request->get('nom_salle');
        $salle->description=$request->get('description');
        $salle->etat =$request->get('etat');
        $salle->save();
        return redirect('/ListeSalle')->with('success', 'salle updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salle  $salle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $salle = Salle::find($id);
        $salle->delete();

        return redirect('/ListeSalle')->with('salle', 'Salle deleted!');
    }

}
