<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Event;
use App\Utilisateur;
use App\Salle;
use Illuminate\Http\Request;

class UtilisateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $utilisateur = Utilisateur::all(); 
        return view('SalleReunion.ListeEntreprise', compact('utilisateur'));
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create()
    {
        $utilisateur = Utilisateur::all(); 
        return view('SalleReunion.ListeEntreprise', compact('utilisateur'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
{
    $request->validate([
        'nom_entreprise'=>'required',
        'email'=>'required',
        'login'=>'required',
        'code'=>'required'
    ]);

    $utilisateur = new utilisateur([
        'nom_entreprise' => $request->get('nom_entreprise'),
        'email' => $request->get('email'),
        'login' => $request->get('login'),
        'code' => $request->get('code')
    ]);
    $utilisateur->save();
    
    return redirect('ListeEntreprise')->with('success', 'utilisateur saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function show(utilisateur $utilisateur)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $utilisateur = Utilisateur::find($id);
        return view('SalleReunion.edit', compact('utilisateur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $utilisateur = Utilisateur::find($id);
        $utilisateur->nom_entreprise =  $request->get('nom_entreprise');
        $utilisateur->email = $request->get('email');
        $utilisateur->login = $request->get('login');
        $utilisateur->code = $request->get('code');
        $utilisateur->save();

        return redirect('ListeEntreprise')->with('success', 'utilisateur updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\utilisateur  $utilisateur
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $utilisateur = Utilisateur::find($id);
        $utilisateur->delete();

        return redirect('/ListeEntreprise')->with('utilisateur', 'Utilisateur deleted!');
    }
}
