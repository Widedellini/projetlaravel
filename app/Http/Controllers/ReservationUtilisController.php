<?php

namespace App\Http\Controllers;

use App\ReservationUtilis;
use App\Utilisateur;
use App\Event;
use App\Salle;
use Illuminate\Http\Request;

class ReservationUtilisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('SalleReunion.GesReserSalleEntr', compact('reservation'));
    }


    public function form()
    {
        $salle = Salle::all();
        $utilisateur = Utilisateur::all();
        return view('SalleReunion.Reservation', compact('reservation','event','salle','utilisateur'));
    }

public function test()
    {
      echo"edfg";die;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $event = Event::all(); 
        $salle = Salle::all();
        $utilisateur = Utilisateur::all();
       return view('SalleReunion.Reservation', compact('event','salle','utilisateur'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $request->validate([
            'date_debut'=>'required',
            'heure_debut'=>'required',
            'date_fin'=>'required',
            'heure_fin'=>'required',
            'type'=>'required',
            'salle_id'=>'required',
            'utilisateur_id'=>'required'
             
        ]);

        $event = new Event([
            'date_debut' => $request->get('date_debut'),
            'heure_debut' => $request->get('heure_debut'),
            'date_fin' => $request->get('date_fin'),
            'heure_debut' => $request->get('heure_debut'),
            'heure_fin' => $request->get('heure_fin'),
            'salle_id' => $request->get('salle_id'),
            'utilisateur_id' => $request->get('utilisateur_id')
        ]);
        $event->save();
        
        return redirect('SalleReunion.Reservation')->with('success', 'event saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
}
