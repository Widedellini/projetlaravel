<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Calendar;
use App\Utilisateur;
use App\Salle;
use App\Event;

class EventController extends Controller

{

    

    public function index()

    {

       $events = [];

       $data = Event::all();

       if($data->count()){

          foreach ($data as $key => $value) {

            $events[] = Calendar::event(

                $value->heure_debut,

                true,

                new \DateTime($value->start_date),

                new \DateTime($value->end_date.' +1 day')

            );

          }

       }

      $calendar = Calendar::addEvents($events); 

      return view('SalleReunion.mycalender', compact('calendar'));

    }


    public function form()
      {
        $event = Event::all(); 
        $salle = Salle::all();
        $utilisateur = Utilisateur::all();
        return view('SalleReunion.Reservation', compact('reservation','event','salle','utilisateur'));
    }


    public function create()
    {
        $event = Event::all(); 
        $salle = Salle::all();
        $utilisateur = Utilisateur::all();
       return view('SalleReunion.Reservation', compact('event','salle','utilisateur'));
    }


  public function store(Request $request)
    {
   $request->validate([
            'nom_salle'=>'required',
            'nom_entreprise'=>'required',
            'date_début'=>'required',
            'heure_début'=>'required',
            'date_fin'=>'required',
            'heure_fin'=>'required'
        ]);

        $event = new event([
            'nom_salle' => $request->get('nom_salle'),
            'nom_entreprise' => $request->get('nom_entreprise'),
            'date_début' => $request->get('date_début'),
            'heure_début' => $request->get('heure_début'),
            'date_fin' => $request->get('date_fin'),
            'heure_fin' => $request->get('heure_fin')
        ]);
        $event->save();
        echo"ert";die;
        return redirect('SalleReunion.mycalender')->with('success', 'event saved!');
    }

}
