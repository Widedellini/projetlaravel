<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Event;
use App\Utilisateur;
use App\Salle;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('SalleReunion.Reservation', compact('reservation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reservation = Reservation::all(); 
       return view('SalleReunion.Reservation', compact('reservation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
   $request->validate([
            'nom_salle'=>'required',
            'nom_entreprise'=>'required',
            'date_début'=>'required',
            'heure_début'=>'required',
            'date_fin'=>'required',
            'heure_fin'=>'required'
        ]);

        $event = new event([
            'nom_salle' => $request->get('nom_salle'),
            'nom_entreprise' => $request->get('nom_entreprise'),
            'date_début' => $request->get('date_début'),
            'heure_début' => $request->get('heure_début'),
            'date_fin' => $request->get('date_fin'),
            'heure_fin' => $request->get('heure_fin')
        ]);
        $event->save();
        
        return redirect('alleReunion.Reservation')->with('success', 'event saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        //
    }
}
