<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{

protected $fillable = [
        'nom_entreprise',
        'email',  
        'login',  
        'code'    
    ];    
public function event(){
    return $this->hasMany(Event::class);
}
}
