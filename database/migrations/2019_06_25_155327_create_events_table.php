<?php

use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('events', function (Blueprint $table) {

            $table->increments('id');

            $table->string('title');

            $table->date('date_debut');

            $table->time('heure_debut');

            $table->date('date_fin');

            $table->time('heure_fin');
            
            $table->boolean('type');

            $table->unsignedBigInteger('salle_id');
            $table->foreign('salle_id')->references('id')->on('salles');

            $table->unsignedBigInteger('utilisateur_id');
            $table->foreign('utilisateur_id')->references('id')->on('utilisateurs'); 

            $table->timestamps();

        });

    }

    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::drop("events");

    }

}
